import { useState } from "react";
import { Button, FormControl, Input, Spinner } from "@chakra-ui/react";
import { useMutation } from "@apollo/client";
import { ADD_TODO, ALL_TODO } from "../apollo/todos";

const AddTodo = () => {
  const [text, setText] = useState("");
  const [addTodo, { loading, error }] = useMutation(ADD_TODO, {
    // refetchQueries sends request every time we add todoItem and receive whole array again
    // refetchQueries: [{ query: ALL_TODO }],

    // update existing cache with new todoItem
    update(cache, { data: { newTodo } }) {
      // extract todos from cache
      const { todos } = cache.readQuery({ query: ALL_TODO });
      // update cache with old and newTodo
      cache.writeQuery({
        query: ALL_TODO,
        data: {
          todos: [newTodo, ...todos],
        },
      });
    },
  });

  const handleAddTodo = () => {
    if (text.trim().length) {
      addTodo({
        variables: {
          title: text,
          userId: 123,
          completed: false,
        },
      });
      setText("");
    }
  };

  const handleKey = (event) => {
    if (event.key === "Enter") handleAddTodo();
  };

  if (loading) return <Spinner />;
  if (error) return <h2>Error...</h2>;

  return (
    <FormControl display={"flex"} mt={6}>
      <Input
        value={text}
        onChange={(e) => setText(e.target.value)}
        onKeyPress={handleKey}
      />
      <Button onClick={handleAddTodo}>Add todo</Button>
    </FormControl>
  );
};

export default AddTodo;
