import { VStack, Spinner } from "@chakra-ui/react";
import TodoItem from "./TodoItem";
import TotalCount from "./TotalCount";
import { useQuery, useMutation } from "@apollo/client";
import { ALL_TODO, DELETE_TODO, UPDATE_TODO } from "../apollo/todos";

const TodoList = () => {
  const { loading, error, data } = useQuery(ALL_TODO);
  const [toggleTodo, { error: updateError }] = useMutation(UPDATE_TODO);
  const [deleteTodo, { error: removeError }] = useMutation(DELETE_TODO, {
    // updating existing cache
    update(cache, { data: { removeTodo } }) {
      // modify cache with modify() method
      cache.modify({
        // modifying fields such allTodos
        fields: {
          // allTodos() method is used by server, that's why I use it to update all existing todos in cache
          allTodos(currentTodos = []) {
            // filtering existing todos by id in __ref (link to todos)
            return currentTodos.filter(
              (todo) => todo.__ref !== `Todo:${removeTodo.id}`
            );
          },
        },
      });
    },
  });

  if (loading) {
    return <Spinner />;
  }

  if (error || updateError || removeError) {
    return <h2>Error...</h2>;
  }

  return (
    <>
      <VStack spacing={2} mt={4}>
        {data.todos.map((todo) => (
          <TodoItem
            key={todo.id}
            onToggle={toggleTodo}
            onDelete={deleteTodo}
            {...todo}
          />
        ))}
      </VStack>
      <TotalCount />
    </>
  );
};

export default TodoList;
