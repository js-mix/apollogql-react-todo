module.exports = {
    tabWidth: 4,
    semi: true,
    singleQuote: true,
    printWidth: 120,
    bracketSpacing: false,
    endOfLine: 'auto',
};